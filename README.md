# Apache Connector for Crowd
The connector is no longer supported by Atlassian as of 31 December 2014.
The repository has been moved [here](https://bitbucket.org/atlassianlabs/cwdapache). 